# Dashboard - Adventure Works - with Power BI

Adventure Works is a fictional company that sells bicycles, sports clothing and accessories. This file contains a dashboard that displays the company's Key Performance Indicators and additional information that can help the firm's decision-making process. The dashboard contains three different tabs: an executive summary, a product tab and a customer tab. The dimensions used to create this dashboard are the following: 1250 pixels (width) and 680 pixels (height). Please, update the page size in your Power BI software, so you can see the dashboard with the correct dimensions.


This dashboard was created during the course "Microsoft Power BI Desktop for Business Intelligence" by Maven Analytics in Udemy. This experience helped me to develop new skills to use Power BI for Data Analytics. Link to see the course: https://www.udemy.com/course/microsoft-power-bi-up-running-with-power-bi-desktop/
